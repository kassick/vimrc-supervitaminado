" Vim script
" File: "pandoc.vim"
" Created: "Qua, 09 Set 2015 15:41:56 -0300 (kassick)"
" Updated: "Qua, 04 Nov 2015 15:26:54 -0200 (kassick)"
" $Id$
" Copyright (C) 2015, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br> 

setlocal breakindent
setlocal formatoptions=tcq2njl
nmap <leader>pd :silent Pandoc beamer<cr>
