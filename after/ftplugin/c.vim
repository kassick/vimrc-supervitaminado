" Vim script
" File: "c.vim"
" Created: "Qua, 09 Set 2015 15:32:39 -0300 (kassick)"
" Updated: "Seg, 25 Jan 2016 13:50:06 -0200 (kassick)"
" $Id$
" Copyright (C) 2015, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br> 

setlocal formatoptions=tcroq2njl
setlocal cin
