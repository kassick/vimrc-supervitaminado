"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim script
" File: ".vimrc"
" Updated: "Ter, 24 Mai 2016 20:50:01 -0300 (kassick)"
" Copyright (C) 2015, Rodrigo Virote Kassick rvkassick@inf.ufrgs.br
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim configuration file
" Help: ":help option
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

call dirsettings#Install()

set nocompatible

if filereadable(expand("~/.vim/vimrc.bundles"))
  source ~/.vim/vimrc.bundles
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General Options:
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" syntax on  " on overrides any highligh made on the bundles
syntax enable " enable syntax highlighting
set nocompatible " set a lot of options
set autoindent
set backspace=indent,eol,start
set backup
set display=lastline
"set formatoptions=tcroq2njl
set hlsearch
set incsearch
set linebreak
set breakat=\ ^I!@*-+;:,./\?
set nolist
set listchars=tab:��,trail:�,extends:>,precedes:<
"set listchars=eol:�,tab:��,trail:�,extends:>,precedes:<
set showbreak=>

" Set breakindent in the filetypes that need it
"set breakindent

" Matchpairs should be set by each filetype plugin
"set matchpairs=(:),[:],{:},<:>

set matchtime=3
set showmatch
set pastetoggle=<F7>
set report=1
set shell=bash
set shortmess=atTIoO
set ignorecase
set smartcase
set smarttab
"set tabstop=4
set softtabstop=4
set shiftwidth=4
set ts=8
set expandtab
set textwidth=78
set wrapmargin=8
set wrap
set nostartofline
set switchbuf=useopen,split
set tags=./tags,tags
set title
set ttyscroll=3
set wildmenu
set wildmode=list:longest
set winheight=5
set writebackup
set showcmd
set history=100
set ruler
set number
set modeline
set belloff=all
" set complete=.,w,b,u,t,i,k
"              .,k,b,u,i,t
" set dictionary+=/usr/share/dict/words
set spelllang=pt
set confirm
set guioptions=agim
" set path=/usr/include,/usr/local/include,.
set splitbelow
set splitright
set restorescreen
set sessionoptions=blank,buffers,curdir,folds,help,options,winsize,globals
"set viminfo=!,'50,\"100,h,%
set viminfo='10,\"100,:20,%,n~/.viminfo 
set browsedir=current

" swap, backup and backup skip directories
set dir=$HOME/tmp/
set bdir=$HOME/tmp/
set bsk=$HOME/tmp/

set colorcolumn=80
set laststatus=2

" Auto-wrapping for vim
source ~/.vim/wrap.vim

"my stuff
let g:disable_go_to_filepath=0


" From http://vim.wikia.com/wiki/Append_output_of_an_external_command 
command! -nargs=* -complete=shellcmd R new | setlocal buftype=nofile bufhidden=hide noswapfile | r !<args> 

" c support
let g:C_LocalTemplateFile		= $HOME . '/.vim/c-support/templates/Templates'
let g:C_LocalTemplateDir		= fnamemodify( g:C_LocalTemplateFile, ":p:h" ).'/'

" YouCompleteMe
let g:ycm_global_ycm_extra_conf = '~/.vim/ycm_conf.py'
"let g:ycm_ycmd_python_interpreter = '/usr/bin/python'
"let g:ycm_server_python_interpreter = '/usr/bin/python'
"let g:ycm_complete_in_comments = 1
"let g:ycm_collect_identifiers_from_comments_and_strings = 1
"let g:ycm_collect_identifiers_from_tags_files = 1
"let g:ycm_add_preview_to_completeopt = 1
"let g:ycm_autoclose_preview_window_after_insertion = 1
"let g:ycm_enable_diagnostic_signs = 0
"let g:ycm_cache_omnifunc = 0
"let g:ycm_server_keep_logfiles = 1
"let g:ycm_server_log_level = 'debug'
let g:ycm_always_populate_location_list = 1
let g:ycm_filetype_blacklist = {
      \ 'tagbar' : 1,
      \ 'qf' : 1,
      \ 'notes' : 1,
      \ 'markdown' : 0,
      \ 'unite' : 1,
      \ 'text' : 1,
      \ 'vimwiki' : 1,
      \ 'pandoc' : 1,
      \ 'infolog' : 1,
      \ 'mail' : 1,
      \ 'tex' : 1,
      \ 'lisp' : 0,
      \ 'elisp' : 0,
      \}

nnoremap <leader>ycgd :YcmCompleter GoToDefinition<cr>
nnoremap <leader>ycgc :YcmCompleter GoToDeclaration<cr>
nnoremap <leader>ycgt :YcmCompleter GoTo<cr>
nnoremap <leader>ycgi :YcmCompleter GoToInclude<cr>
nnoremap <leader>ycgg :YcmCompleter GoToImprecise<cr>

" Ultisnip
let g:UltiSnipsExpandTrigger = '<c-l>'
let g:UltiSnipsJumpForwardTrigger = '<c-j>'
let g:UltiSnipsJumpBackwardTrigger = '<c-k>'
let g:UltiSnipsListSnippets = '<c-h>'

" VimWIki
let g:vimwiki_list = [{'path': '~/Dropbox/WorkDiary/', 'path_html': '~/Dropbox/WorkDiary_html/'}]

" Vim pandoc
let g:pandoc#keyboard#sections#header_style='s'

 

" key mappings

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Calendar and Tag List
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <silent> <F11> :TagbarToggle<CR>�
"nnoremap <silent> <F12> :TlistSync<CR>
nnoremap <silent> <C-F10> :BufExplorer<CR>�
let g:bufExplorerFindActive=0



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim Notes
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{

let g:notes_directories = ['~/Dropbox/Notes']
let g:notes_suffix = '.md'

" }}}


"""
"Ignore help
nmap <F1>  :echo "Ignoring help. You're welcome"<cr>
imap <F1>  <esc>a

" Copy/Paste integration
imap <C-v> <space><esc>"+gPs
imap <C-c> "+ya
"nmap <C-v> a <esc>"+gPx   -- normal c-v -> selection
vmap <C-c> "+y
vnoremap p <Esc>:let current_reg = @"<CR>gvs<C-R>=current_reg<CR><Esc> " p in visual mode


" this conflicts with easymotion
"noremap <silent> <Leader>w :call ToggleWrap()<CR>

if has("gui_running")
	set mousehide " Hide the mouse when typing text
	colorscheme jellybeans
        set bg=light
        "set guifont=Anonymous\ Pro\ for\ Powerline\ 12
        set guifont=Liberation\ Mono\ 12

        " work around linux gui F10 problem by making it a nice safe refresh
        if has("gui_gnome") || has("gui_gtk") || has("gui_gtk2")
            map <F10> <c-L>
        endif
else
	colorscheme jellybeans
        set bg=dark

	map <C-F11>:set mouse=<cr>
	map <C-F12>:set mouse=a<cr>
	imap <C-F11><esc>:set mouse=<cr>i
	imap <C-F12><esc>:set mouse=a<cr>i
endif

if has("mouse")
	set mouse=a
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Autocommands in events
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if has("autocmd")

    " Automatically open, but do not go to (if there are errors) the
    " quickfix / location list window, or close it when is has become
    " empty.
    " 
    " Note: Must allow nesting of autocmds to enable any customizations for
    " quickfix buffers.  Note: Normally, :cwindow jumps to the quickfix
    " window if the command opens it (but not if it's already open).
    " However, as part of the autocmd, this doesn't seem to happen.

    autocmd QuickFixCmdPost [^l]* nested cwindow
    autocmd QuickFixCmdPost    l* nested lwindow
    autocmd QuickFixCmdPost *grep* cwindow
    autocmd BufEnter * call GoToFilePath()
    autocmd BufRead,BufNewFile * call ToggleWrapInit()
    "autocmd BufRead,BufNewFile *.wiki call ToggleWrapTrue()
	
    filetype plugin indent on " enable file type detection.

    " the cursor always jump to the last known position
    au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif

    "au BufEnter *.tex,*.latex,*.sty,*.cls call FormatParagraphMap()
    "au BufEnter *.txt call FormatParagraphMap()
    "au BufEnter *.md call FormatParagraphMap()
    
    " fdoc is yaml
    autocmd BufRead,BufNewFile *.fdoc setlocal filetype=yaml
    " md is markdown
    "autocmd BufRead,BufNewFile *.md setlocal filetype=markdown
    "autocmd BufRead,BufNewFile *.md setlocal spell
    "autocmd BufRead,BufNewFile *.tex call LaTexMap()
    "autocmd BufRead,BufNewFile *.c,*.C,*.cpp,*.h,*.H,*.hpp map ,in :set ff=unix<cr>:w!<cr>:!astyle --options=$HOME/.vim/astylerc % <cr>:edit %<cr>:w!<cr>
    
    
    " This needs to be here for some reason
    " pandoc, for some reason, does not like my formatoptions set in either
    " ftplugin/markdown.vim or after/ftplugin/markdown.vim
    "autocmd BufRead,BufNewFile *.tex set filetype=latex
    "autocmd BufNewFile,BufFilePost,BufRead *.md set formatoptions=tcq2njl
    "augroup pandoc_syntax
    "    au! BufNewFile,BufFilePRe,BufRead *.md set filetype=markdown.pandoc
    "augroup END


endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Navegar pelas janelas splitadas
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <s-down>   <c-w>j<c-w>_
nmap <s-up>     <c-w>k<c-w>_
nmap <s-left>   <c-w>h<c-w>|
nmap <s-right>  <c-w>l<c-w>|



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Navegar pelos buffers abertos
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"map  <silent> <s-tab>  <Esc>:if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>
"imap  <silent> <s-tab>  <Esc>:if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>



"" Omnifunc
"autocmd FileType python set omnifunc=pythoncomplete#Complete
"autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
"autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
"autocmd FileType css set omnifunc=csscomplete#CompleteCSS
"autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
"autocmd FileType php set omnifunc=phpcomplete#CompletePHP
"autocmd FileType c set omnifunc=ccomplete#Complete


"if has('python')
"    echo 'HAS PYTHON \o/'
"else
"    echo 'NO HAZ PYTHON <o>'
"endif







function! GoToFilePath()
        if (g:disable_go_to_filepath == 1)
            return
        endif

	let path = expand("%:p:h")
	if (len(path) > 0) && (path !~ "^/tmp") && (path !~ '^[a-z]\+://') 
		let path = substitute(path,"\ ","\\\\ ","g")
		exe "lcd ".path
	endif
endfunction

function! BigFont()
    let g:original_font=&guifont
    set guifont=Anonymous\ Pro\ for\ Powerline\ 38
endfunction

function! NormalFont()
    if exists("g:original_font")
        exe("set guifont=" . escape(g:original_font, ' '))
    endif
endfunction



if filereadable(expand("~/.vim/vimrc.local"))
  source ~/.vim/vimrc.local
endif
