" Vim script
" File: "/home/kassick/Utils/vimrc-supervitaminado.svn/vim/plugin/info.vim"
" Created: "Sex, 25 Jul 2003 17:08:59 -0300 (caciano)"
" Updated: "Sáb, 03 Jul 2010 11:55:28 -0300 (kassick)"

"your own info
let User=$USER
let UserName="Rodrigo Virote Kassick"
let UserMail="rvkassick@inf.ufrgs.br"
let UserNickname="kazic"
let UserHomepage="http://www.inf.ufrgs.br/~rvkassick"

"abbreviation
iab rvk <esc>:let @j = g:UserName<cr>:normal "jp<cr>a
iab kzk <esc>:let @j = g:UserNickname<cr>:normal "jp<cr>a
iab rkm <esc>:let @j = g:UserMail<cr>:normal "jp<cr>a

