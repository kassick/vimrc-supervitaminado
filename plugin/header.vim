"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim script
" File: "header.vim"
" Created: "Thu, 23 May 2002 00:09:59 -0300 (caciano)"
" Updated: "Sex, 18 Jul 2014 19:46:02 -0300 (kassick)"
" Copyright (C) 2002, Rodrigo Kassick <rvkassick@inf.ufrgs.br>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" - inserts headers accoring to the filetype in new files.
" - modifies time stamp and file name when writing files.
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

command! -nargs=? Header call Header (<q-args>)

autocmd BufNewFile * call Header ("")
autocmd BufWritePre,FileWritePre * call HeaderTimeStamp ()

" create a RFC822-conformant date
fun! RFC822()
	return strftime("%a, %d %b %Y %H:%M:%S %z")
endfun

function! HeaderTimeStamp ()
        let s:file = expand("%:t")

        if expand("%:p") =~ '^fugitive:'
            return
        endif

        "if match(s:file, "^fugitive:")
            "echo "Not updateing"
            "unlet s:file
            "return
        "endif
        "unlet s:file

	let save_report = &report
	let &report = 999999
	mark k

	" substitute the file name
	let currentline = line(".")
	let pattern = '^\(.*\<File:\)\s\+"\([^"]*\)"'
	exec '%s�'.pattern.'�&�ge'
	if line(".") != currentline
		normal ''
		exec 'ijump! /'.pattern.'/'
		exec 's�'.pattern.'�\1 "'.escape(expand("%:f"), '\').'"�e'
	endif

	" time stamp
	let currentline = line(".")
	let pattern = '^\(.*\)\<\(Time-stamp:\|Last Modified:\|Last Modification:\|Last Update:\|Updated:\)\s\+"\([^"]*\)"'
	exec '%s�'.pattern.'�&�ge'
	if line(".") != currentline
		normal ''
		exec 'ijump! /'.pattern.'/'
		exec 's�'.pattern.'�\1\2 "'.RFC822().' ('.g:User.')"�e'
	endif

	" the following is for the maintenance of vim syntax files.
	" If you don't maintain a syntax file you can remove this.
	if "vim" == &ft
		let currentline = line(".")
		let pattern = '^\("\s\+Last Change:\).*$'
		exec '%s�'.pattern.'�&�ge'
		if line(".") != currentline
			normal ''
			exec 'ijump! /'.pattern.'/'
			exec 's�'.pattern.'�\1\t'.RFC822().'�e'
		endif
	endif
	" remove up to here, if you don't maintain a vim syntax file.

	normal 'kz.
	let &report = save_report
endfunction

function! Header (...)
	if a:1 == ""
		let xft = &ft
	else
		let xft = a:1
	endif

	let com     = ""
	let trailer = ""
	if "c" == xft
            " C and Cpp have templates for that
            return
	"	let leader  = "/* C source code"
	"	let com     = " * "
	"	let trailer = " */\n"
	elseif "cpp" == xft
            " C and Cpp have templates for that
            return
            "	let leader  = "// C++ source code"
	elseif "sh" == xft
		let leader  = "#!/bin/sh"
	elseif "python" == xft
                let leader  = "#!/usr/bin/env python\n# -*- coding: utf-8 -*-\n"
                let com     = "# "
	elseif "perl" == xft
		let leader  = "#!/usr/bin/perl"
	elseif "make" == xft
		let leader  = "# Makefile"
		let trailer = "\nBASE = ".expand("%:p:h:t")."\ninclude ~/make/c.mk\n"
	elseif "vim" == xft
		let leader  = '" Vim script'
	elseif xft == "tex"
		let leader  = "% LaTex source code"
	elseif xft == "java"
		let leader  = "// "
	elseif xft == "js" || xft == "javascript"
		let leader  = "// Java script"
	elseif "gnuplot" == &ft
		let leader  = "#!/usr/bin/gnuplot -persist"
	elseif "html" == xft || "php3" == xft
		call HtmlHeader()
		return
	else
		"echo "unknown header"
            return
	endif

	if "" == com
		if 0 == match (leader, "#!")
			let com = "# "
		else
			let com = substitute(leader, '\(\S\+\)\s.*', '\1', "") . " "
		endif
	end

	let ctime = RFC822()

	let @k = leader."\n"
	let @k = @k . com . "File: \"\"\n"
	let @k = @k . com . "Created: \"" . ctime . " (" . g:User . ")" . "\"\n"
	let @k = @k . com . "Updated: \"\"\n"
	let @k = @k . com . "$Id$\n"
	let @k = @k . com . "Copyright (C) " . strftime("%Y") . ", " . g:UserName . " <" . g:UserMail . "> \n"
	let @k = @k . trailer

	0

	normal "kP


	call HeaderTimeStamp ()
	set modified
endfunction


function! HtmlHeader ()
0insert
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<!--
 ! Rodrigo Kassick <rvkassick@inf.ufrgs.br>
 ! Created: __DATE__
 ! Updated: __DATE__
 ! $Id:$
 !-->
<html>

<head>
	<title>__TITLE__</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rev=Made href="mailto:__MAILTO__">
	<link rel=Start href="./">
	<link rel=Copyright href="__HREF__">
	<meta name="generator" content="Vim-6.1">
	<meta name="author" content="__USERNAME__, __MAILTO__">
	<meta name="description" content="__DESCRIPTION__">
	<meta name="keywords" content="__KEYWORDS__">
	<link rel="stylesheet" href="css/cmach.css" type="text/css">
</head>


<body text="#C0C0C0" bgcolor="#330000" link="#33CC00" vlink="#006600" alink="#66FF99">
	<hr>
	__TEXT__
	<hr>
	<address>
		<b><u><font size=+1>__USERNAME__</u></font> - 
		<a href="mailto:__MAILTO__"</a></b>    
	</address>
	
	<p align="right"><i><u>Last update: __DATE__ </u></i></p>
</body>
</html>
.

exe '%s/__DATE__/'.RFC822().'/g'
exe '%s/__USERNAME__/' . g:UserName.'/g'
exe '%s/__MAILTO__/'   . g:UserMail . '/g'
exe '%s/__HREF__/'     . escape(g:UserHomepage,'/~') . '/g'
exe ''
endfunction
