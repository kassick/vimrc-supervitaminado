# ViM configuration files and script collection

By kàzic, based on Caciano Machado's early setup

## Initial Setup

*  Clone into ~/.vim and branch
*  ln -s ~.vim/vimrc ~/.vimrc
*  run  `git submodule init ; git submodule update`
*  run `vim -c BundleUpdate`
*  install exuberant-ctags
*  install astyle
*  run `tar -xzvf fonts.tgz -C ~` to extract powerline enabled fonts to the right place.
*  run `fc-cache -vf ~/.fonts`

## Personalization

Clone it and customize it away. `vimrc.local` is supposed to keep site-local configurations (in case you use more than one machine), but can be used for any configuration you see fit.

Some changes that I recomend doing:

*  Edit plugin/header.vim   to change copyright and contact information
*  Edit plugin/info.vim  to change abbreviations
*  See available colorschemes on colors and on the bundles (i.e. jellybeans, dinsinguished and vividchalk) and make vim any color you like
*  Edit astylerc to change options passed to the identation tool (shortcut  ,in )

## Plugins included

### YouComPleteMe

YCM is a powerful C++ and python and C# (and something else?) completer for vim. It needs to compile a helper library. Go on .vim/bundle/YouCompleteMe and run
```
./install.sh --clang-completer
```

In Fedora 20, I needed to install libstdc++-static and execute
```
./install.sh --clang-completer --use-system-clang
```

My setup uses a python script (~/.vim/ycm_conf.py) to load compilations parameters. It provides automatically the following flags:

*  System include files, needed by libclang in some cases
*  The flags contained in a .clang_complete file IF one is present in the same directory as the source file. Example .clang_complete file:
```
-D__USE_GNU -I/path/to/my/lib/include --std=c++0x
```
*  If the project is a CMake Project, the compilation database generated by cmake (`cmake -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE`)


### Fugitive (GIT plugin)

Provides Ex commands Gstatus, Gcommit, Gwrite, Gread, Gdiff , etc. Pretty powerfull, check the documentation.


### Vundle

Prefered way of installing new plugins is via vundle. Add 
```
Bundle 'git-url or github repo name'
```
to vimrc.bundles

### Powerline

Powerline adds some cuteness and some useful information to VIM statusline. It provides a modified tabbar as well, but it does not work very well (as of august 2014). To disable it, copy `powerline_config.json` to `~/.config/powerline/config.json`

It also depends on a set of modified fonts and a fontconfig entry, all available in `fonts.tgz`. Extract this file in your home directory and run `fs-cache -vf ~/.fonts`. If the powerline status bar still looks odd and your fonts ackward, you may need to restart the X server. For more intormation, see [Powerline Documentation](http://powerline.readthedocs.org/en/latest/installation/linux.html).

# Useful shortcuts

* *NORMAL* ` ,in` : For C and C++, runs the code through astyle to indent everything.
* *NORMAL* `C-F10` : Opens bufexplorer
* *NORMAL* `F11` : Opens taglist
* `\\w`, `\\e`, etc : Easymotion movements
* *INSERT* `C-X o` : Omnicomplete, usually provided by YouCompleteMe
* `C-l` : Expands snipets shown by YCM
* *VISUAL* `\nr` : Opens the selection in a new editable buffer (NrrwRgn)
* Python file mode, *NORMAL* `\r` : Run
