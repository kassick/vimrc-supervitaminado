" Vim script
" vim:set et sw=4 ts=8 tw=78:
" File: "wrap.vim"
" Created: "Qua, 09 Set 2015 15:09:11 -0300 (kassick)"
" Updated: "Seg, 09 Mai 2016 12:49:08 -0300 (kassick)"
" $Id$
" Copyright (C) 2015, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br> 

func! ChangeFormatParagraph()
  if !exists("b:fmtpar")
    let b:fmtpar=0
  endif

  if b:fmtpar==1
    setlocal formatoptions-=a
    let b:fmtpar=0
    echo "Paragraph Auto-Formatting OFF"
  else
    setlocal formatoptions+=a
    let b:fmtpar=1
    echo "Paragraph Auto-Formatting ON"
  endif
endfunc

func! TexFormatParagraph()
	let col = virtcol(".")
	let lin = line(".")

	exec "normal!{gq}|"
	"Justify('tw',3,0)

	execute lin
	execute "normal!" col ."|"
endfunc
function! ToggleWrapInit()
    let b:local_magic_wrap = 0
    let b:local_set_wrap = 0
    let b:local_virtualedit = ""

    if &l:fo =~ 't'
      let b:local_magic_wrap_fo_has_t = 1
    else
      let b:local_magic_wrap_fo_has_t = 0
    endif

endfunction

function! ToggleWrapTrue()
    let b:local_magic_wrap = 0
    call ToggleWrap()
endfunction

function! ToggleWrap()

  if b:local_magic_wrap == 1
    echo "Wrap OFF"
    if b:local_set_wrap == 1
        setlocal wrap
    else
        setlocal nowrap
    endif
    if b:local_breakindent == 1
        setlocal breakindent
    else
        setlocal nobreakindent
    endif
    exe "setlocal virtualedit=" . b:local_virtualedit
    silent! nunmap <buffer> <Up>
    silent! nunmap <buffer> <Down>
    silent! nunmap <buffer> <Home>
    silent! nunmap <buffer> <End>
    silent! iunmap <buffer> <Up>
    silent! iunmap <buffer> <Down>
    silent! iunmap <buffer> <Home>
    silent! iunmap <buffer> <End>
    let b:local_magic_wrap = 0
    if b:local_magic_wrap_fo_has_t == 1
        setlocal fo+=t
    endif
  else
    echo "Wrap ON"
    
    if &wrap
        let b:local_set_wrap = 1
    else
        let b:local_set_wrap = 0
    endif

    let b:local_virtualedit = &virtualedit
    let b:local_breakindent = &breakindent

    setlocal wrap
    setlocal linebreak
    setlocal nolist
    setlocal virtualedit=
    setlocal breakindent
    setlocal display+=lastline
    noremap  <buffer> <silent> <Up>   gk
    noremap  <buffer> <silent> <Down> gj
    noremap  <buffer> <silent> <Home> g<Home>
    noremap  <buffer> <silent> <End>  g<End>
    inoremap <buffer> <silent> <Up>   <C-o>gk
    inoremap <buffer> <silent> <Down> <C-o>gj
    inoremap <buffer> <silent> <Home> <C-o>g<Home>
    inoremap <buffer> <silent> <End>  <C-o>g<End>
    let b:local_magic_wrap = 1
    if &l:fo =~ 't'
      let b:local_magic_wrap_fo_has_t = 1
    else
      let b:local_magic_wrap_fo_has_t = 0
    endif
    setlocal fo-=t
  endif
endfunction

func! TextDisableAutoWrap()
    if &l:fo =~ 't'
      let b:TextHadAutoWrap = 1
    else
      let b:TextHadAutoWrap = 0
    endif

    set fo-=t
endfunc

func! TextEnableAutoWrap()
    if b:TextHadAutoWrap == 1
        setlocal fo+=t
    endif
    unlet b:TextHadAutoWrap
endfunc
