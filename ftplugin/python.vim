" Vim script
" File: "ftplugin/python.vim"
" Created: "Qua, 09 Set 2015 14:57:28 -0300 (kassick)"
" Updated: "Qua, 09 Set 2015 15:11:04 -0300 (kassick)"
" $Id$
" Copyright (C) 2015, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br> 

 
setlocal formatoptions=tcroq2njl
"Options for python-mode
let g:pymode_folding = 0
let g:pymode_lint = 0
" Already got jedi and ycm
let g:pymode_rope = 0
let g:pymode_rope_completion = 0

"SympylFold
let g:SimpylFold_docstring_preview=1


"Use fakegir cache to have completition by jedi
"https://github.com/strycore/fakegir
python << EOF
import vim, os, sys
try:
    pypath = os.environ['PYTHONPATH']
except:
    pypath = ""

os.environ['PYTHONPATH'] = (pypath+':' if len(pypath) > 0
                            else "" ) \
                            + os.environ["HOME"] + "/.cache/fakegir"
EOF
