source ~/.vim/ftplugin/plaintext.vim

let g:tex_flavor='latex' "fix a bug http://stackoverflow.com/questions/21648238/vim-youcompleteme-enable-tex-filetype
setlocal matchpairs=(:),[:],{:}

"let g:LatexBox_custom_indent=0
let g:LatexBox_quickfix=4
let g:LatexBox_show_warnings=0
let g:LatexBox_Folding=1
let g:LatexBox_fold_envs=0
let g:LatexBox_fold_preamble=1


"func! LaTexMap() " syntax and editing issues for LaTex files
	" ,dvi = create and open dvi file
        "map ,dvi :w<cr>:!latex %<cr>:!xdvi %<.dvi<cr>
        "map ,ltx :w<cr>:!latex %<cr><cr>
        "map ,lpdf :w<cr>:!pdflatex %<cr><cr>
        "" ,ps = create and open ps file
        "map ,ps :w<cr>:!latex %<cr>:!dvips -t a4 %<.dvi -o %<.ps<cr><cr>
        "" ,pdf = create and open pdf file
        "map ,pdf :w<cr>:!latex %<cr>:!latex %<cr>:!dvipdf %<.dvi %<.pdf<cr><cr>
        "" cleans tex archives:
        "map ,cls :w<cr>:!rm -f %<.aux %<.log %<.bbl %<.blg %<.toc<cr><cr>
        "map ,dcls :w<cr>:!rm -f %<.dvi %<.ps %<.pdf *.{aux,log,bbl,blg,bak} *~<cr><cr>
	"imap ;ol \overline{}<esc>i
	"imap ;bp \bigoplus{}<esc>i
	"imap ;tt \texttt{}<esc>i
	"imap ;tb \textbf{}<esc>i
	
	"imap ;em \emph{}<esc>i
	"imap ;ti \textit{}<esc>i

	" colocar abreviaturas para simbolos
	"ab ... \ldots
	"ab <= \leqstant
	"ab >= \getslantlk
	"ab ~= \thickapprox
	"ab -> \rightarrow
	"ab <- \leftarrow

	"let fdir = expand("%:h")
	"if fdir != ""
	"	lcd %:h
	"endif

	"set formatprg=
	"runtime macros/justify.vim

	"map  <C-F> :call TexFormatParagraph()<cr>
	"foldpimap <C-F> <esc>:call TexFormatParagraph()<cr>a
        "nmap <C-P> }kV{jJo<esc>

	"let g:Tex_SmartKeyQuote = 0
	"let g:Tex_UseSimpleLabelSearch=1
	"let g:Tex_BIBINPUTS="*.bib"
        "set formatoptions-=l

        "set mousemodel=popup
        "vmenu PopUp.Comments.Out :s/^/%/<CR>:let @/ = ""<CR>
        "vmenu PopUp.Comments.In  :s/^%//<CR>:let @/ = ""<CR>

        "do LatexSuite User LatexSuiteFileType
        " Fix other quirks, add other mappings
        " This needs to be here due to ordering -- if I do this in texrc,
        " latexsuite will override ~~ mapping
        "call IMAP("LLI","\\item \<++>",'tex')
        "call IMAP('EEI','\item[<++>] <++>','tex')
        "call IMAP(';fr','\begin{frame}\frametitle{<++>}<++>\end{frame}','tex')
        "call IMAP(';blk','\begin{block}{<++>}<++>\end{block}','tex')
        "call IMAP("~~", "~~", "tex")
"endfunc

imap <buffer> [[     \begin{
imap <buffer> ]]     <Plug>LatexCloseCurEnv
nmap <buffer> <F5>   <Plug>LatexChangeEnv
vmap <buffer> <F7>   <Plug>LatexWrapSelection
vmap <buffer> <S-F7> <Plug>LatexEnvWrapSelection
imap <buffer> ((     \eqref{
        
setlocal  iskeyword+=:,_

function! Synctex()
    " Get basename, as zathura needs basename rather than full path, but gives
    " us full path in %{input}
    " Also, change it to the corresponding pdf
    if !exists("g:syncpdf")
        if exists("b:main_tex_file")
            let g:syncpdf = b:main_tex_file
        else
            let g:syncpdf = expand("%")
        endif
    endif
    let syncpdf_short = split(g:syncpdf, '/')[-1]
    let syncpdf_short = split(syncpdf_short, '\.')[0]
    let syncpdf_short = syncpdf_short . '.pdf'
        
    " remove 'silent' for debugging
    execute "silent !zathura --synctex-forward " . line('.') . ":" . col('.') . ":'" . expand('%') . "' '" . syncpdf_short . "'"
endfunction
map <C-enter> :call Synctex()<cr>

