" Vim script
" File: "c.vim"
" Created: "Qua, 09 Set 2015 15:02:53 -0300 (kassick)"
" Updated: "Ter, 10 Mai 2016 15:09:56 -0300 (kassick)"
" $Id$
" Copyright (C) 2015, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br> 

" map ,in :set ff=unix<cr>:w!<cr>:!astyle --options=$HOME/.vim/astylerc % <cr>:edit %<cr>:w!<cr>

nnoremap <buffer><Leader>cf :<C-u>ClangFormat<CR>
vnoremap <buffer><Leader>cf :ClangFormat<CR>
" if you install vim-operator-user
"map <buffer><Leader>x <Plug>(operator-clang-format)
setlocal foldmethod=marker
