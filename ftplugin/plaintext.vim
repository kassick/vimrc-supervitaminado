" Vim script
" File: "plaintext.vim"
" Created: "Qua, 09 Set 2015 15:01:33 -0300 (kassick)"
" Updated: "Dom, 13 Set 2015 23:34:44 -0300 (kassick)"
" $Id$
" Copyright (C) 2015, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br> 

setlocal spell
setlocal formatoptions=tcq2njl
setlocal breakindent

" Format a paragraph with ctrl-f
map  <C-F> :call TexFormatParagraph()<cr>
imap <C-F> <esc>:call TexFormatParagraph()<cr>a
"mapping to join all lines in a paragraph
nmap <C-P> }kV{jJo<esc>

" Use vim visual wrapping instead of autofmt. I can always C-F the text if I
" want wrapped lines
imap <F8> <esc>:call ToggleWrap()<cr>a
map <F8> :call ToggleWrap()<cr>

"let g:ycm_min_num_of_chars_for_completion=99 "avoid overeager ycm
let b:did_load_pt=1
