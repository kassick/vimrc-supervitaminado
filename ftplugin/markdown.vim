" Vim script
" File: "markdown.vim"
" Created: "Qua, 09 Set 2015 14:50:21 -0300 (kassick)"
" Updated: "Qua, 09 Set 2015 15:45:42 -0300 (kassick)"
" $Id$
" Copyright (C) 2015, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br> 

source ~/.vim/ftplugin/plaintext.vim
setlocal formatoptions=tcq2njl
